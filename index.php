<?php 
	/*
	Software Name: php-ide
	Software URI: http://www.arunwebsolus.com
	Description: php-ide is a browser based php ide that allows you to write and execute php scripts directly from the
	browser.
	Version: 1.0
	Author: Millo Lailang
	Author Email: sunoy14socmed@gmail.com
	Author URI: http://www.arunwebsolus.com 
	License: GPLv2
	*/
	
	//define constant for current working directory
	define('CUR_DIR', getcwd());
	
	//if the user has submitted his code from the code editor
	if(isset($_REQUEST['form_submit'])){
		$code = $_REQUEST['text_field'];
		//Create a new temporary file
		$open_file = fopen(CUR_DIR . '/test9.php', 'w');
		//Write the user's submitted php code into the new temp file
		fwrite($open_file, $code);
		//Close the temporary file
		fclose($open_file);
	}
?>

<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
	<textarea name="text_field" placeholder="Enter php script here" style="width:900px; height: 350px; background-color: #ddd;"><?php echo $code; ?></textarea><br /><br />
	<input type="submit" name="form_submit" value="Test" style="width: 100px; height: 30px;" />
</form>

<?php 
	//Include the temporary file created to display the user created content on the browser
	require_once(CUR_DIR . '/test9.php');
	
	//Delete the temporary file
	unlink(CUR_DIR . '/test9.php');
?>